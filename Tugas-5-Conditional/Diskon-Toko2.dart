import 'dart:io';

void main() {
  stdout.write('ID Beli : ');
  String idBeliwiwi = stdin.readLineSync()!;

  if (idBeliwiwi.isEmpty) {
    print('ID Beli ini wajib diisi.');
    return;
  }

  stdout.write('Kode Barang : ');
  String kodeBarangwiwi = stdin.readLineSync()!;

  if (kodeBarangwiwi.isEmpty) {
    print('Kode Barang ini wajib diisi.');
    return;
  }

  stdout.write('Nama Barang : ');
  String namaBarangwiwi = stdin.readLineSync()!;

  if (namaBarangwiwi.isEmpty) {
    print('Nama Barang ini wajib diisi.');
    return;
  }

  stdout.write('Harga Barang : ');
  String? hargaBarangInputwiwi = stdin.readLineSync();
  if (hargaBarangInputwiwi == null || hargaBarangInputwiwi.isEmpty) {
    print('Harga Barang harus diisi.');
    return;
  }

  hargaBarangInputwiwi = hargaBarangInputwiwi.replaceAll(',', ''); 
  double? hargaBarangwiwi = double.tryParse(hargaBarangInputwiwi);

  if (hargaBarangwiwi == null || hargaBarangwiwi <= 0) {
    print('Harga Barang harus diisi dengan angka yang valid.');
    return;
  }

  stdout.write('Jumlah Beli : ');
  int? jumlahBeliwiwi = int.tryParse(stdin.readLineSync()!);

  if (jumlahBeliwiwi != null && jumlahBeliwiwi > 0) {
    double jumlahBayarwiwi = hargaBarangwiwi * jumlahBeliwiwi;
    double diskonwiwi = 0.0;
    String diskonInfowiwi = '';

    if (jumlahBayarwiwi > 5000000) {
      diskonwiwi = jumlahBayarwiwi * 0.10;
      diskonInfowiwi = '10%';
    } else if (jumlahBayarwiwi > 1000000) {
      diskonwiwi = jumlahBayarwiwi * 0.07;
      diskonInfowiwi = '7%';
    } else if (jumlahBayarwiwi > 400000) {
      diskonwiwi = jumlahBayarwiwi * 0.05;
      diskonInfowiwi = '5%';
    } else if (jumlahBayarwiwi > 200000) {
      diskonwiwi = jumlahBayarwiwi * 0.03;
      diskonInfowiwi = '3%';
    }

    double totalBayarwiwi = jumlahBayarwiwi - diskonwiwi;

    print('\nID Beli\t\t: $idBeliwiwi');
    print('Kode Barang\t: $kodeBarangwiwi');
    print('Nama Barang\t: $namaBarangwiwi');
    print('Harga Barang\t: $hargaBarangwiwi');
    print('Jumlah Beli\t: $jumlahBeliwiwi');
    print('Jumlah Bayar\t: $jumlahBayarwiwi');
    print('Diskon\t\t: $diskonInfowiwi');
    print('Total Bayar\t: $totalBayarwiwi');
  } else {
    print('Jumlah Beli harus diisi dengan angka yang valid dan lebih besar dari 0.');
  }
}