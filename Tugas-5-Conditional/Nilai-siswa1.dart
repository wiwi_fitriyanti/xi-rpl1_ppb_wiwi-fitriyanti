import 'dart:io';

void main () {
stdout.write('Nama siswa: ');
  var namaSiswawiwi = stdin.readLineSync() ?? '';
  if (namaSiswawiwi.isEmpty) {
    print("mohon isi nama siswa");
    return;
  }

  stdout.write('Kelas Siswa: ');
  var kelasSiswawiwi = stdin.readLineSync() ?? '';
  if (kelasSiswawiwi.isEmpty) {
    print("mohon isi kelas siswa");
    return;
  }

  stdout.write("Nilai Harian: ");
  double nilaiHarianwiwi = double.parse(stdin.readLineSync()!);
  
  stdout.write("Nilai PTS: ");
  double nilaiPTSwiwi = double.parse(stdin.readLineSync()!);
  
  stdout.write("Nilai Praktek: ");
  double nilaiPraktekwiwi = double.parse(stdin.readLineSync()!);
  
  stdout.write("Nilai PAS: ");
  double nilaiPASwiwi = double.parse(stdin.readLineSync()!);

  double nilaiAkhirwiwi = (nilaiHarianwiwi + nilaiPTSwiwi + nilaiPraktekwiwi + nilaiPASwiwi) / 4;

  var statusSiswawiwi = (nilaiAkhirwiwi >= 78)
      ? "Lulus, naik fase berikutnya"
      : "Remidial, bisa naik fase berikutnya setelah menyelesaikan fase sebelumnya";

  print("Nilai Akhir: $nilaiAkhirwiwi");
  print("Status Siswa: $statusSiswawiwi");
}