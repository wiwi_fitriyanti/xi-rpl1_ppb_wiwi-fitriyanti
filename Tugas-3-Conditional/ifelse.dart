import 'dart:io';
void main(){
stdout.write("Masukkan nama: ");
String? namawiwi = stdin.readLineSync();

  if (namawiwi?.isEmpty == true) {
    print("Nama harus diisi!");
  } else {
    stdout.write("Masukkan peran: ");
    String? peranwiwi = stdin.readLineSync();

    if (peranwiwi?.isEmpty == true) {
      print("Halo $namawiwi, Pilih peranmu untuk memulai game!");
    } else if (peranwiwi == "penyihir") {
      print("Selamat datang di Dunia Werewolf, $namawiwi");
      print("Halo penyihir $namawiwi, kamu dapat melihat siapa yang menjadi werewolf!");
    } else if (peranwiwi == "guard") {
      print("Selamat datang di Dunia Werewolf, $namawiwi");
      print("Halo guard $namawiwi, kamu akan melindungi temanmu dari serangan werewolf!");
    } else if (peranwiwi == "werewolf") {
      print("Selamat datang di Dunia Werewolf, $namawiwi");
      print("Halo werewolf $namawiwi, Kamu adalah seorang werewolf! Coba habiskan semua pemain.");
    } else {
      print("Peran yang kamu pilih tidak tersedia. Pilih peran yang sesuai: penyihir, guard, atau werewolf.");
    }
  }
}